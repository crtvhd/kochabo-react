var Loader = {
    lines: 13,
    length: 0,
    width: 2,
    radius: 4,
    corners: 1.0,
    color: '#000',
    speed: 1,
    trail: 70,
    hwaccel: true,
    top: '50%',
    left: '50%'
}

module.exports = Loader