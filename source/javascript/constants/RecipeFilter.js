var RecipeFilterAttributes = [
  'all', 
  'meat', 
  'fish', 
  'veggie', 
  'vegan', 
  'glutenfree', 
  'lactosefree'
]

module.exports = RecipeFilterAttributes