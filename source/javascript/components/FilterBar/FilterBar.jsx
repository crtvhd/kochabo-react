var React = require('react');

var FilterBar = React.createClass({
  propTypes: {
    resultsCount : React.PropTypes.number.isRequired
  },
  render: function(){
    return (
      <div className="filterbar filterbar--horizontal">
        <div className="filterbar__container">
          <div className="filterbar__list-container">
            <span className="filterbar__label">Rezepte filtern:</span>
            {this.props.children}
          </div>
          <div className="filterbar__counter-container">
            <strong>{this.props.resultsCount}</strong> Rezepte werden angezeigt
          </div>
        </div>
      </div>
    )
  }
});

module.exports = FilterBar