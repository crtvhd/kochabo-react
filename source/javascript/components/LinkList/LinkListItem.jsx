var React = require('react');

var LinkListItem = React.createClass({
  propTypes: {
    label   : React.PropTypes.string.isRequired,
    active  : React.PropTypes.bool
  },
  render: function(){
    active = this.props.active ? 'linklist__link--active' : ''
    return (
      <li className="linklist__item">
        <a href="#" 
          className={"linklist__link " + active} 
          onClick={this.props.onClick}>{this.props.label}</a>
      </li>
    )
  }
});

module.exports = LinkListItem