var React         = require('react');
var LinkListItem  = require('./LinkListItem.jsx');

var LinkList = React.createClass({
  propTypes: {
    items      : React.PropTypes.array.isRequired,
    activeItem : React.PropTypes.string.isRequired
  },
  _handleClick: function(i){
    this.props.onSelect(this.props.items[i])
  },
  render: function(){
    return (
      <ul className="linklist">
        {this.props.items.map(function(item, i) {
          return <LinkListItem
                    label={item}
                    active={this.props.activeItem == item}
                    onClick={this._handleClick.bind(this, i)}
                    key={i}/>
        }, this)}
      </ul>
    )
  }
});

module.exports = LinkList