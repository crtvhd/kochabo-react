var React  = require('react');
var Recipe = require('./Recipe.jsx')

var RecipesGrid = React.createClass({
  render: function(){
    recipes = this.props.recipes
    return (
      <div className="recipe-grid">
        {recipes.map(function(recipe) {
          return (
            <div className="recipe-container" key={recipe.attributes.id}>
              <Recipe recipe={recipe.attributes} />
            </div>
          )
        })}
      </div>
    )
  }
});

module.exports = RecipesGrid