_     = require('lodash')
React = require('react')
Ribbon = require('./Ribbon.jsx')

var Recipe = React.createClass({
  render: function(){
    recipe = this.props.recipe
    ribbonComponent = null

    if (Number(recipe.duration) <= 30) {
      ribbonComponent = <Ribbon body="Schnelles Gericht!"/>
    }
    
    properties = recipe.properties.map(function(property){ 
      return <li className={"properties__item property__item--" + property} key={property}>{property}</li> 
    })

    return (
      <div className="recipe recipe--has-ribbon recipe--has-hover">
        <div className="recipe__header">
          <a href="#" className="recipe__thumbnail">
            <img src={recipe.imageBig} className="recipe__image"/>
          </a>
          {ribbonComponent}
          <ul className="properties">
            {properties}
          </ul>
        </div>
        <div className="recipe__body">
          <a href="#" className="recipe__title">{recipe.name}</a>
          <div className="recipe__meta">
            <ul className="ulist ulist--inline">
              <li className="ulist__item"><i className="ulist__icon fa fa-clock-o"/>{recipe.duration} Minuten</li>
              <li className="ulist__item"><i className="ulist__icon fa fa-leaf"/>{recipe.ingredientsCount} Zutaten</li>
            </ul>
          </div>
        </div>
      </div>
    )
  }
});

module.exports = Recipe