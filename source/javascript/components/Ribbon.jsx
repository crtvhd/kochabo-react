var React = require('react');

var Ribbon = React.createClass({
  render: function(){
    body = this.props.body
    return (
      <div className="ribbon">
        <div className="ribbon__fold-up"></div>
        <div className="ribbon__body">{body}</div>
        <div className="ribbon__fold-back"></div>
      </div>
    )
  }
});

module.exports = Ribbon