var React = require('react');

var Pagination = React.createClass({
  propTypes: {
    label               : React.PropTypes.string.isRequired,
    onPaginate          : React.PropTypes.func.isRequired,
    autoPaginate        : React.PropTypes.bool,
    autoPaginateOffset  : React.PropTypes.number,
    disabled            : React.PropTypes.bool
  },
  _handleScroll: function(event) {

    viewportHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0)
    scrollPosition = document.getElementsByTagName("body")[0].scrollTop
    scrollOffset   = this.props.autoPaginateOffset || 0

    bodyRect    = document.body.getBoundingClientRect()
    elemRect    = document.querySelectorAll(".pagination")[0].getBoundingClientRect()
    elemOffset  = elemRect.top - bodyRect.top

    if ((scrollPosition + viewportHeight + scrollOffset) > elemOffset){
      this.props.onPaginate()
    }
  },
  componentDidMount: function(){
    if(this.props.autoPaginate)
      window.onscroll = document.ontouchmove = _.debounce(this._handleScroll, 250, { maxWait : 1000 })
  },
  componentWillUnmount: function(){
    if(this.props.autoPaginate)
      window.onscroll = document.ontouchmove = null
  },
  render: function(){
    var classes = this.props.disabled ? 'button-disabled' : ''
    return (
      <div className="pagination">
        <div className="pagination__container">
          <button onClick={this.props.onPaginate} className={classes}>{this.props.label}</button>
        </div>
      </div>
    )
  }
});

module.exports = Pagination