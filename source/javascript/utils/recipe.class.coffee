class Recipe

  constructor: (@attributes = {}) ->
    @extendAttributes()
    
  isUpcoming: ->
    Number(@attributes.lastUsedYear) >= moment().year() and Number(@attributes.lastUsedCw) > moment().week()
    
  toJSON: ->
    json =
      isUpcoming: @isUpcoming()
    _.extend json, @attributes
    json
  
  extendAttributes: ->
    @setBoxType()
    @setProperties()
  
  setBoxType: ->
    @attributes.boxType = switch 
      when @attributes.isVegan  then "vegan"
      when @attributes.isVeggie then "veggie"
      when @attributes.isFit    then "fit"
      else "classic"
    
  setProperties: ->
    @attributes.properties = []
    
    propertiesMapping = 
      fish: "isFish"
      meat: "isMeat"
      vegetarian: "isVegetarian"
      vegan: "isVegan"
      glutenfree: "isGlutenFree"
      lactosefree: "isLactoseFree"
    
    @attributes.properties.push key for key, attr of propertiesMapping when @attributes[attr] is true

window.Recipe = Recipe