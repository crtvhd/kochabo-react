function serializeObject(obj) {
  return Object.keys(obj).map(function(key) {
      return key + '=' + obj[key];
  }).join('&');
}

module.exports = serializeObject