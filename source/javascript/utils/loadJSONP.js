serializeObject = require('./serializeObject')

var loadJSONP = (function loadJSONP_outer( window, document, undefined ) { 
  var uuid, head, main_script

  uuid = 0
  head = document.head || document.getElementsByTagName( 'head' )[0]
  main_script = document.createElement( 'script' )
  main_script.type = 'text/javascript'
 
  return function loadJSONP_inner( url, params, callback, context ) { var name, script
    params = serializeObject(params)

    name = '__jsonp_' + uuid++
    if ( url.match(/\?/) )
      url += params + '&callback=' + name
    else
      url += '?' + params + '&callback=' + name

    script = main_script.cloneNode()
    script.src = url

    window[name] = function( data ) {
      callback.call( ( context || window ), data )
      head.removeChild( script )
      script = null
      delete window[name]
    }

    head.appendChild( script )
  }
})( window, document )

module.exports = loadJSONP