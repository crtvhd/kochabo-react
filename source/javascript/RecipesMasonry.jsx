React         = require('react')
Loader        = require('react-loader')

loaderOptions = require('./constants/loader')

RecipeClass   = require('./utils/recipe.class')
loadJSONP     = require('./utils/loadJSONP')

LinkList      = require('./components/LinkList/LinkList.jsx')
FilterBar     = require('./components/FilterBar/FilterBar.jsx')
Pagination    = require('./components/Pagination.jsx')
RecipesGrid   = require('./components/RecipesGrid.jsx')
RecipeFilter  = require('./constants/RecipeFilter.js')


var RecipesMasonry = React.createClass({
  getInitialState: function(){
    return { 
      loaded      : false, 
      recipes     : [],
      api         : "http://api.kochabo.at/api/recipes.php",
      apiParams   : {
        type        : "all",
        pagesize    : 20,
        page        : 1,
        recipetype  : "all"
      },
      pagination : {
        label    : "Mehr Rezepte laden",
        disabled : false
      }
    }
  },
  componentDidMount: function(){
    this.fetchRecipes(this.state.apiParams, function(recipes){
      this._fetchRecipesSuccess(recipes)
    }.bind(this))
  },
  fetchRecipes: function( params, callback ){
    loadJSONP(this.state.api, params, function(data) {
      if(data.error){
        this._fetchRecipesFail({
          status: "Error",
          statusText: data.error
        })
        return false;
      }
      recipes = data.recipes.map(function(recipe){ 
        return new Recipe(recipe) 
      })
      callback(recipes)
    }.bind(this))
  },
  _fetchRecipesSuccess: function (recipes) {
    this.setState({
      recipes: recipes,
      loaded: true
    })
  },
  _fetchRecipesFail: function (error) {
    alert( "Request Failed: " + error.status + " (" + error.statusText + ")")
  },
  _onFilter: function(keyword) {
    this.setState({
      recipes: [],
      loaded: false
    })
    updatedApiParams = _.merge(this.state.apiParams, { recipetype: keyword, page: 1 })
    this.fetchRecipes( updatedApiParams, function(recipes){
      this._fetchRecipesSuccess(recipes)
    }.bind(this))
  },
  _onPaginate: function() {
    this.setState({ 
      pagination : {
        label: "Rezepte werden geladen…",
        disabled : true
      } 
    })
    updatedApiParams = _.merge( this.state.apiParams, { page: (this.state.apiParams.page + 1) })
    this.fetchRecipes( updatedApiParams, function(recipes){
      this._fetchRecipesSuccess(this.state.recipes.concat(recipes))
    }.bind(this))

    setTimeout(function(){
      this.setState({
        pagination : {
          label: "Mehr Rezepte laden",
          disabled : false
        }
      })
    }.bind(this), 500)
  },
  render: function(){
    return (
      <div>
        <FilterBar resultsCount={this.state.recipes.length}>
          <LinkList 
            items={RecipeFilter}
            onSelect={this._onFilter} 
            activeItem={this.state.apiParams.recipetype} />
        </FilterBar>
        <Loader loaded={this.state.loaded} options={loaderOptions}>
          <RecipesGrid recipes={this.state.recipes} />
          <Pagination
            key={this.state.apiParams.page}
            label={this.state.pagination.label}
            disabled={this.state.pagination.disabled}
            onPaginate={this._onPaginate}
            autoPaginate={true}
            autoPaginateOffset={800}/>
        </Loader>
      </div>
    )
  }
});

module.exports = RecipesMasonry