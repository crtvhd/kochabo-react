var gulp = require('gulp');
var config = require('../config').iconFont;
var browserSync  = require('browser-sync');

gulp.task('iconFont', function() {
  console.log(config.src, config.dest)
  return gulp.src(config.src)
    .pipe(gulp.dest(config.dest))
    .pipe(browserSync.reload({stream:true}));
});
