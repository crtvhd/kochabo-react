var gulp    = require('gulp');
var config  = require('../config').production;
var size    = require('gulp-filesize');
var gzip    = require('gulp-gzip');

gulp.task('gzip', function() {
  return gulp.src([config.jsSrc, config.cssSrc])
    .pipe(gzip())
    .pipe(gulp.dest(config.dest))
    .pipe(size());
});
