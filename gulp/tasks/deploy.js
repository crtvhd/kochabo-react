var gulp  = require('gulp');
var rsync = require('gulp-rsync');
 
gulp.task('deploy', function() {
  return gulp.src('build/**')
    .pipe(rsync({
      root: 'build',
      username: 'martin',
      hostname: 'mrtn.pro',
      destination: '/home/martin/VHOSTS/mrtn.pro/www-kochabo-react'
    }));
});