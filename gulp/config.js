var dest = "./build";
var src = './source';

module.exports = {
  browserSync: {
    server: {
      // Serve up our build folder
      baseDir: dest
    }
  },
  sass: {
    src: src + "/stylesheets/**/*.{sass,scss}",
    dest: dest,
    settings: {
      indentedSyntax  : true,
      errLogToConsole : true,
      outputStyle     : 'expanded',
      imagePath       : 'images',
      includePaths    : [
        './node_modules'
      ]
      .concat(require('node-bourbon').includePaths)
      .concat(require('node-neat').includePaths)
    }
  },
  images: {
    src: src + "/images/**",
    dest: dest + "/images"
  },
  iconFont: {
    src: "./node_modules/font-awesome/fonts/**",
    dest: dest + "/fonts"
  },
  markup: {
    src: src + "/views/**",
    dest: dest
  },
  browserify: {
    // A separate bundle will be generated for each
    // bundle config in the list below
    bundleConfigs: [{
      entries: src + '/javascript/application.coffee',
      dest: dest,
      outputName: 'application.js',
      // Additional file extentions to make optional
      extensions: ['.coffee', '.hbs', '.jsx'],
      // list of modules to make require-able externally
      require: []
    }]
  },
  production: {
    cssSrc: dest + '/*.css',
    jsSrc: dest + '/*.js',
    dest: dest
  }
};
